require 'test_helper'

class VoiceactorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  fixtures :voiceactors

  test 'names_regexp is union of names' do
    assert(/相坂優歌|西明日香/, Voiceactor.names_regexp)
  end

  test 'gender sym is a symbol' do
    assert(:FEMALE, voiceactors(:one).gender_sym)
  end
end
