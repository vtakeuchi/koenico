require 'test_helper'

class LiveProgramTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def self.make_program_info(*args)
    %w(id title description provider_type thumbnail_url
      start_date end_date start_time end_time).zip(args).to_h
  end

  StreamList = [
    make_program_info(
      1000, 'one thousand', '...1000 desc', 'channel', 'http://example.com/t0.jpg',
      '2016/10/05', '2016/10/05', '01:00', '02:00'),
    make_program_info(
      2000, 'two thousand', '...2000 desc', 'channel', 'http://example.com/t1.jpg',
      '2016/10/10', '2016/10/11', '05:00', '06:00'),
    make_program_info(
      3000, 'three thousand', '...3000 desc', 'official', 'http://example.com/t2.jpg',
      '2016/10/20', '2016/10/20', '11:00', '12:00')
  ]

  def timetable_data
    {"timeline" =>
     {"date" => "2016-10-07",
      "stream_list" => StreamList
     }
    }
  end

  def setup_faraday_stub(stub)
    stub.get('/api/getZeroTimeline?date=2016-10-01'){
      [200, {}, JSON.dump(timetable_data)]
    }

    stub.get('/watch/lv1000'){ [ 200, {}, <<-EOT ] }
      <html><body><div id="jsFollowingAdMain">SOME_CONTENT<p id='livetags'>
        <a rel=tag>Tag1</a>
        <a rel=tag>Tag13</a>
      </p></div></body></html>
    EOT

    stub.get('/watch/lv2000'){ [ 200, {}, <<-EOT ] }
      <html><body><div id="jsFollowingAdMain">OTHER_CONTENT西明日香<p id='livetags'>
        <a rel=tag>Tag20</a>
      </p></div></body></html>
    EOT

    stub.get('/watch/lv3000'){ [ 200, {}, <<-EOT ] }
      <html><body><div id="jsFollowingAdMain">XXX_CONTENT<p id='livetags'>
        <a rel=tag>Anime</a>
        <a rel=tag>西明日香</a>
      </p></div></body></html>
    EOT
  end

  def setup
    @faraday = Faraday.new{| f |
      f.adapter :test, Faraday::Adapter::Test::Stubs.new{| stub |
        setup_faraday_stub(stub)
      }
    }
    @program = LiveProgram.new(live_id: 1000)
  end

  test 'crawl timetable and save programs' do
    LiveProgram.crawl_timetable(Date.new(2016, 10, 1),
      faraday: @faraday, livepage_fetch_interval: 0)
    p2000 = LiveProgram.find_by(live_id: 2000)
    assert_equal('OTHER_CONTENT西明日香', p2000.content)
    p3000 = LiveProgram.find_by(live_id: 3000)
    assert_includes(p3000.tags_string, 'Anime')
  end

  test 'load livepage and set content and tags' do
    @program.load_livepage(faraday: @faraday)
    assert_equal('SOME_CONTENT', @program.content)
    assert_equal(%w(Tag1 Tag13), @program.tags_string)
  end

  test 'load livepage and set voiceactor from content' do
    program = LiveProgram.new(live_id: 2000)
    program.load_livepage(faraday: @faraday)
    assert_equal(['西明日香'], program.voiceactors.map(&:name))
  end

  test 'load livepage and set voiceactor from tags' do
    program = LiveProgram.new(live_id: 3000)
    program.load_livepage(faraday: @faraday)
    assert_equal(['西明日香'], program.voiceactors.map(&:name))
  end
end
