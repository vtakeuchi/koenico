require 'test_helper'

class ManageVoiceactorsControllerTest < ActionDispatch::IntegrationTest
=begin
  setup do
    @manage_voiceactor = manage_voiceactors(:one)
  end

  test "should get index" do
    get manage_voiceactors_url
    assert_response :success
  end

  test "should get new" do
    get new_manage_voiceactor_url
    assert_response :success
  end

  test "should create manage_voiceactor" do
    assert_difference('ManageVoiceactor.count') do
      post manage_voiceactors_url, params: { manage_voiceactor: { abstract: @manage_voiceactor.abstract, furigana: @manage_voiceactor.furigana, gender: @manage_voiceactor.gender, name: @manage_voiceactor.name } }
    end

    assert_redirected_to manage_voiceactor_url(ManageVoiceactor.last)
  end

  test "should show manage_voiceactor" do
    get manage_voiceactor_url(@manage_voiceactor)
    assert_response :success
  end

  test "should get edit" do
    get edit_manage_voiceactor_url(@manage_voiceactor)
    assert_response :success
  end

  test "should update manage_voiceactor" do
    patch manage_voiceactor_url(@manage_voiceactor), params: { manage_voiceactor: { abstract: @manage_voiceactor.abstract, furigana: @manage_voiceactor.furigana, gender: @manage_voiceactor.gender, name: @manage_voiceactor.name } }
    assert_redirected_to manage_voiceactor_url(@manage_voiceactor)
  end

  test "should destroy manage_voiceactor" do
    assert_difference('ManageVoiceactor.count', -1) do
      delete manage_voiceactor_url(@manage_voiceactor)
    end

    assert_redirected_to manage_voiceactors_url
  end
=end
end
