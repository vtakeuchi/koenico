require 'test_helper'

class NicoLiveCrawlerTest < ActiveSupport::TestCase
  class LiveProgramMock
    def initialize
      @calls = []
    end

    def crawl_timetable(day, **opts)
      @calls << [day, opts]
    end

    attr_reader :calls
  end

  def make_crawler(mode='week', live_program_klass=LiveProgramMock.new)
    NicoLiveCrawler.new(mode, live_program_klass)
  end

  def test_load_config
    c = make_crawler
    assert_equal({}, c.load_config)
  end

  def test_update_config
    c = make_crawler
    assert_equal({}, c.load_config)
    c.update_config({'configtest-12' => 42})
    assert_equal(42, c.load_config['configtest-12'])
    c.update_config({'configtest-50' => 51})
    assert_equal(42, c.load_config['configtest-12'])
    assert_equal(51, c.load_config['configtest-50'])
  end

  def test_next_offset
    c = make_crawler('month')
    assert_equal(9, c.next_offset(8))
    assert_equal(31, c.next_offset(30))
    assert_equal(8, c.next_offset(31))
  end

  def test_execute
    MiscConfig.save_config(NicoLiveCrawler::ConfigKey, {'far' => 88}) 

    mock = LiveProgramMock.new
    c = make_crawler('far', mock)
    day = Date.parse('2016-8-10')
    c.instance_variable_set(:@today, day)
    c.execute
    assert_equal(
      [ day+88,
        day+89,
        day+90,
        day+32,
        day+33,
        day+34,
        day+35
      ], mock.calls.map(&:first))
  end

  def test_week_cycle
    c = make_crawler('week')
    c.execute
    assert_equal(0, c.load_config['week_cycle'])
    23.times{ c.execute }
    assert_equal(3, c.load_config['week_cycle'])
    40.times{ c.execute }
    assert_equal(2, c.load_config['week_cycle'])
  end

  def test_execute_new_cycle
    mock = LiveProgramMock.new
    c = make_crawler('week', mock)
    100.times{ c.execute }
    assert_equal(
      100.times.map{| i |
        not (i % (8*6) <= 7)
      }, mock.calls.map(&:last).map{|opt| opt[:only_new] })
  end
end
