# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161014031013) do

  create_table "contacts", force: :cascade do |t|
    t.string   "category"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "live_programs", force: :cascade do |t|
    t.integer  "live_id",                       null: false
    t.string   "title"
    t.text     "description"
    t.string   "thumbnail_url"
    t.datetime "start_at",                      null: false
    t.datetime "end_at"
    t.text     "content"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "provider_type"
    t.integer  "voiceactors_count", default: 0, null: false
    t.index ["end_at"], name: "index_live_programs_on_end_at"
    t.index ["live_id"], name: "index_live_programs_on_live_id"
    t.index ["start_at"], name: "index_live_programs_on_start_at"
  end

  create_table "live_programs_voiceactors", id: false, force: :cascade do |t|
    t.integer "live_program_id", null: false
    t.integer "voiceactor_id",   null: false
  end

  create_table "live_tags", force: :cascade do |t|
    t.integer  "live_program_id", null: false
    t.string   "content",         null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["live_program_id"], name: "index_live_tags_on_live_program_id"
  end

  create_table "misc_configs", force: :cascade do |t|
    t.string   "key"
    t.text     "json_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "voiceactors", force: :cascade do |t|
    t.string   "name"
    t.string   "furigana"
    t.integer  "gender"
    t.text     "abstract"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["furigana"], name: "index_voiceactors_on_furigana"
    t.index ["name"], name: "index_voiceactors_on_name"
  end

end
