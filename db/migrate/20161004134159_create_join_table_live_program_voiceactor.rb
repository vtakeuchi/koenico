class CreateJoinTableLiveProgramVoiceactor < ActiveRecord::Migration[5.0]
  def change
    create_join_table :live_programs, :voiceactors do |t|
      # t.index [:live_program_id, :voiceactor_id]
      # t.index [:voiceactor_id, :live_program_id]
    end
  end
end
