class AddIndexToLivePrograms < ActiveRecord::Migration[5.0]
  def change
    add_index :live_programs, :end_at
  end
end
