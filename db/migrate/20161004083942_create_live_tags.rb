class CreateLiveTags < ActiveRecord::Migration[5.0]
  def change
    create_table :live_tags do |t|
      t.references :live_program, foreign_key: true, null: false
      t.string :content, null: false

      t.timestamps
    end
  end
end
