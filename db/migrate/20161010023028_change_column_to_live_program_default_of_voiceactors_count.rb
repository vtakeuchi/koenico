class ChangeColumnToLiveProgramDefaultOfVoiceactorsCount < ActiveRecord::Migration[5.0]
  def up
    change_column_default :live_programs, :voiceactors_count, 0
  end

  def down
    change_column_default :live_programs, :voiceactors_count, nil
  end
end
