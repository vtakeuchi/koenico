class RenameValueToMiscConfig < ActiveRecord::Migration[5.0]
  def up
    rename_column :misc_configs, :value, :json_data
  end

  def down
    rename_column :misc_configs, :json_data, :value
  end
end
