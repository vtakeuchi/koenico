class CreateMiscConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :misc_configs do |t|
      t.string :key
      t.text :value

      t.timestamps
    end
  end
end
