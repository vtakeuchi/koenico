class ChangeColumnToLiveProgram < ActiveRecord::Migration[5.0]
  def up
    change_column_null :live_programs, :voiceactors_count, false, 0
  end

  def down
    change_column_null :live_programs, :voiceactors_count, true
  end
end
