class AddVoiceactorsCountToLivePrograms < ActiveRecord::Migration[5.0]
  def change
    add_column :live_programs, :voiceactors_count, :integer
  end
end
