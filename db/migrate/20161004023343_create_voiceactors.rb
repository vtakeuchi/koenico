class CreateVoiceactors < ActiveRecord::Migration[5.0]
  def change
    create_table :voiceactors do |t|
      t.string :name
      t.string :furigana
      t.integer :gender
      t.text :abstract

      t.timestamps
    end
    add_index :voiceactors, :name
    add_index :voiceactors, :furigana
  end
end
