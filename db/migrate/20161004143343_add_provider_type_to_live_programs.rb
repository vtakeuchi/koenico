class AddProviderTypeToLivePrograms < ActiveRecord::Migration[5.0]
  def change
    add_column :live_programs, :provider_type, :string
  end
end
