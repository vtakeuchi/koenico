class CreateLivePrograms < ActiveRecord::Migration[5.0]
  def change
    create_table :live_programs do |t|
      t.integer :live_id, null: false
      t.string :title
      t.text :description
      t.string :thumbnail_url
      t.datetime :start_at, null: false
      t.datetime :end_at
      t.text :content

      t.timestamps
    end
    add_index :live_programs, :live_id
    add_index :live_programs, :start_at
  end
end
