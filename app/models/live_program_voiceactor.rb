class LiveProgramVoiceactor < ApplicationRecord
  self.table_name = :live_programs_voiceactors

  belongs_to :live_program, counter_cache: :voiceactors_count
  belongs_to :voiceactor
end
