require 'faraday'
require 'nokogiri'
require 'json'
require 'date'
require 'pry-byebug'
require 'niconico-api'

class LiveProgram < ApplicationRecord
  class Error < StandardError; end
  class ConnectionError < Error; end
  class ContentError < Error; end

  NicoLiveHost = 'live.nicovideo.jp'

  has_many :live_tags, autosave: true

  has_many :live_program_voiceactors, autosave: true
  has_many :voiceactors, through: :live_program_voiceactors, autosave: true

  scope :only_voiceactors_on, lambda{
    where('voiceactors_count > 0')
  }

  scope :all_voiceactors_on, lambda{
    only_voiceactors_on.includes(:voiceactors)
  }

  scope :voiceactors_named_on, lambda{| names |
    ids = joins(:voiceactors).where(<<-EOT, names: names).distinct.pluck(:id)
      voiceactors.name in (:names) OR voiceactors.furigana in (:names)
    EOT
    LiveProgram.where(id: ids).includes(:voiceactors)
  }

  scope :tagged, lambda{| tagname |
    joins(:live_tags).
      where({'live_tags.content' => tagname}).
      includes(:voiceactors).
      preload(:live_tags)
  }

  scope :preloaded, lambda{| tags_too = false |
    if tags_too
      includes(:voiceactors, :live_tags)
    else
      includes(:voiceactors)
    end
  }

  scope :old_to_new, lambda{ reorder(start_at: :asc) }
  scope :new_to_old, lambda{ reorder(start_at: :desc) }

  scope :finished, lambda{| ref=Time.now |
    where('end_at < ?', ref)
  }

  scope :unfinished, lambda{| ref=Time.now |
    where('? <= end_at', ref)
  }

  scope :past_in, lambda{| period,ref=Time.now |
    where('? <= end_at', ref-period)
  }

  scope :search_missing_voiceactor, lambda{| name |
    r = joins(:live_tags)
    r.merge(LiveTag.where(content: name)).or(
      r.where('live_programs.content like ?', "%#{name}%")
    ).distinct
  }

  # OBSOLETE
  def self.crawl_timetable(date, only_new: false, faraday: nil, livepage_fetch_interval: 2)
    faraday ||= Faraday.new(url: "http://live.nicovideo.jp"){| f |
      f.adapter :net_http_persistent
    }
    fetch_timetable(date, faraday: faraday).each{| program |
      begin
        if only_new and not program.new_record?
          logger.info{ "skipping livepage #{program.live_id}" }
          next
        end
        logger.info{ "loading livepage #{program.live_id}" }
        program.load_livepage(faraday: faraday)
      rescue ContentError
        logger.error{ "failed to parse content for #{program}" }
      end
      program.save
      sleep livepage_fetch_interval
    }
  end

  def self.crawl_timetable2(date, only_new: false)
    faraday = Faraday.new(url: "http://live.nicovideo.jp"){| f |
      f.adapter :net_http_persistent
    }

    programs_on_timetable = fetch_timetable(date, faraday: faraday)
    if only_new
      programs_to_update = programs_on_timetable.select(&:new_record?)
      logger.info{ "skipping #{programs_on_timetable.size - programs_to_update.size} programs" }
    else
      programs_to_update = programs_on_timetable
    end
    logger.info{ "updating #{programs_to_update.size} programs" }
    return if programs_to_update.empty?

    update_programs(programs_to_update, faraday: faraday)
  end


  def self.update_programs(programs_to_update, faraday:)
    # XXX: awful
    key_titles = programs_to_update.map{| program | program.title.scan(
      /[\p{Hiragana}ー]{2,}|[\p{Katakana}ー]{2,}|[一-龠々]{2,}|[A-Za-z]{2,}/
    )}.flatten.uniq

    from,to = programs_to_update.map{| program | program.start_at }.minmax

    fetched = {}
    key_titles.each_slice(20){| titles |
      logger.info{ "query with q: #{titles.inspect}" }
      fetched.merge!(NiconicoAPI.hash_from_result(
        NiconicoAPI.query_nicolive_api(%w(title), titles, from: from, to: to, logger: logger)))
    }
    logger.info{ "fetch from API: #{fetched.size} programs" }

    programs_to_update.each{| program |
      begin
        program.load_from_niconico_api_data(fetched.fetch(program.live_id))
        logger.info{ "loaded from API: #{program.live_id}" }
      rescue KeyError
        logger.warn{ "not in API data: #{program.live_id}" }
        begin
          logger.info{ "loading livepage #{program.live_id}" }
          program.load_livepage(faraday: faraday)
          sleep 2
        rescue ContentError
          logger.error{ "failed to parse content for #{program}" }
        end
      end
      program.save!
    }
  end

  # download timetable and return Array of LiveProgram
  def self.fetch_timetable(date, faraday:)
    data = JSON.load(faraday.get(
      '/api/getZeroTimeline?date=%d-%02d-%02d' % [
        date.year, date.month, date.day
      ]
    ).body)

    fetched = data.fetch('timeline').fetch('stream_list').map{| item |
      begin
        [item.fetch('id'), item]
      rescue KeyError
        nil
      end
    }.compact.to_h

    stored = self.where('live_id in (?)', fetched.keys).map{| program |
      [program.live_id, program]
    }.to_h

    fetched.map{| live_id,item |
      begin
        program = stored[live_id] || self.new(live_id: live_id)
        %w(title description thumbnail_url provider_type).map{| key |
          program[key] = item.fetch(key)
        }
        program.start_at = "#{item.fetch('start_date')} #{item.fetch('start_time')}".in_time_zone('Tokyo')
        program.end_at = "#{item.fetch('end_date')} #{item.fetch('end_time')}".in_time_zone('Tokyo')
        program
      rescue KeyError
        nil
      end
    }.compact
  rescue KeyError
    raise ContentError
  end

  def self.crawl_from_search
    names_all = Voiceactor.joins(:live_programs).distinct.pluck(:name)
    names_all += %w(声優 アニメ ボイス)  # XXX: ad-hoc
    logger.info("searching programs with #{names_all.size} names")
    names_all.each_slice(50){| names |
      fetched = NiconicoAPI.hash_from_result(
          NiconicoAPI.query_nicolive_api(NiconicoAPI::Targets_Keyword, names, logger: logger))
      logger.info{ "fetched #{fetched.size} programs" }
      #(fetched.keys - LiveProgram.where('live_id IN (?)', fetched.keys).pluck(:live_id)).tap{| found |
      fetched.keys.tap{| found |
        logger.info{ "creating or updating #{found.size} programs from search" }
      }.each{| live_id |
        program = LiveProgram.find_or_initialize_by(live_id: live_id)
        program.load_from_niconico_api_data(fetched[live_id])
        pp program
      }
    }
  end

  def load_from_niconico_api_data(data)
    hash = {}

    unless self.live_id
      if /\Alv(\d+)/ =~ data['contentId']
        hash[:live_id] = $1.to_i
      else
        return nil
      end
    end

    unless self.title
      hash[:title] = data['title']
    end

    unless self.thumbnail_url
      hash[:thumbnail_url] = data['thumbnailUrl']
    end

    unless self.provider_type
      hash[:provider_type] = data['providerType']
    end

    #unless self.start_at
    new_start_at = Time.zone.parse(data['startTime'])
    unless self.start_at and self.start_at == new_start_at
      hash[:start_at] = Time.zone.parse(data['startTime'])
      hash[:end_at] = hash[:start_at] + 1.hours # XXX: set properly
    end

    hash[:content] = Nokogiri::HTML(data['description']).text

    unless self.description
      content = hash[:content].gsub(/\s+/, '')
      if content.length <= 100
        hash[:description] = content
      else
        hash[:description] = content[0,100] + '…'
      end
    end


    tags_string = data['tags'].split

    assign_attributes(hash)
    update_tags(tags_string)
    self.voiceactors = (
      (Voiceactor.names_set & tags_string) |
        self.content.gsub(/\s+/, '').scan(Voiceactor.names_regexp)
    ).map{| name |
      Voiceactor.find_by_name(name)
    }
    save!
  end

  # fetch livepage and find voiceactors and store data
  def load_livepage(faraday:)
    doc = Nokogiri::HTML(faraday.get("/watch/lv#{live_id}").body)
    desc_container = doc.css('#jsFollowingAdMain').first
    tags_node = desc_container && desc_container.css('#livetags').first
    raise ContentError unless tags_node

    tag_index = desc_container.children.index(tags_node)
    if tag_index
      desc_nodes = desc_container.children[0...tag_index]
    else
      desc_nodes = desc_container.children
    end

    self.content = desc_nodes.text
    update_tags(tags_node.css('a[rel=tag]').map{| tagnode | tagnode.text.strip })

    self.voiceactors = (
      (Voiceactor.names_set & self.tags_string) |
        self.content.gsub(/\s+/, '').scan(Voiceactor.names_regexp)
    ).map{| name |
      Voiceactor.find_by_name(name)
    }
  end

  def tags_string
    self.live_tags.map(&:content)
  end

  def url
    "http://live.nicovideo.jp/watch/lv#{live_id}"
  end

  def started?(ref_time=Time.now)
    start_at <= ref_time
  end

  def finished?(ref_time=Time.now)
    end_at <= ref_time
  end

  def onair?(ref_time=Time.now)
    started?(ref_time) and not finished?(ref_time)
  end

  def start_in?(seconds, ref_time=Time.now)
    started?(ref_time + seconds)
  end

  private

  def update_tags(tags_string)
    new_tags = tags_string.map{| s |
      self.live_tags.find_or_initialize_by(content: s)
    }
    (self.live_tags - new_tags).each(&:destroy)
    self.live_tags = new_tags
  end
end
