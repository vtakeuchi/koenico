class MiscConfig < ApplicationRecord
  serialize :json_data, JSON

  def self.load_config(key)
    find_by(key: key.to_s)&.json_data
  end

  def self.save_config(key, value)
    find_or_initialize_by(key: key.to_s).tap{| record |
      record.json_data = value
      record.save
    }
  end

=begin
  def get_value
    if self.json_data
      JSON.parse(self.json_data)
    else
      nil
    end
  end

  def set_value(value)
    self.json_data = JSON.dump(value)
    self
  end
=end
end
