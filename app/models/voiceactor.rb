class Voiceactor < ApplicationRecord

  GENDER_UNKNOWN = 0
  GENDER_MALE = 1
  GENDER_FEMALE = 2

  Genders = {
    0 => :UNKNOWN,
    1 => :MALE,
    2 => :FEMALE
  }

  after_commit{ self.class.reset_cache_key }

  has_many :live_program_voiceactors, autosave: true
  has_many :live_programs, through: :live_program_voiceactors,
    autosave: true


  class << self
    def fetch_cache(type, expires_in: 24.hours, &block)
      Rails.cache.fetch(cache_key(type), expires_in: expires_in, &block)
    end

    def cache_key(type)
      reset_cache_key unless @cache_key_base
      "#{@cache_key_base}/#{type}"
    end

    def reset_cache_key
      @cache_key_base = "Voiceactor/#{Time.now.to_i}"
      self
    end

    def friendly_data
      fetch_cache(:friendly_data){
        logger.info('friendly_data cache expired')
        cols = %i(name furigana gender)
        self.pluck(*cols).map{| row |
          cols.zip(row).to_h
        }
      }
    end

    def names
      fetch_cache(:names){ self.pluck(:name) }
    end

    def names_regexp
      if self.names.count == 0
        /\z(empty)\A/
      else
        Regexp.new(self.names.map{| s | Regexp.quote(s)}.join('|'))
      end
    end

    def names_set
      Set.new(self.names)
    end

    def import_errata_from_yaml(yaml)
      messages = []
      YAML.load(yaml).each{| data |
        name = data[:name]
        unless Voiceactor.find_by(name: name)
          v = Voiceactor.create(data)

          messages << "add voiceactor: #{name}"
          LiveProgram.search_missing_voiceactor(name).each{| program |
            program.voiceactors += [v]
            program.save
            messages << "  add program lv#{program.live_id}"
          }
        end
      }
      messages
    end
  end

  def gender_sym
    Genders[gender]
  end
end
