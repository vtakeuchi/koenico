require 'date'
require 'json'

class NicoLiveCrawler
  #def fetch_record
  #  MiscConfig.find_or_initialize_by(key: 'CrawlTimeline')
  #end

  ConfigKey = :CrawlTimeline

  def load_config
    #@config = fetch_record.get_value || {}
    @config = MiscConfig.load_config(ConfigKey) || {}
  end

  def update_config(hash)
    #record = fetch_record
    #record.set_value((record.get_value || {}).merge(hash))
    #record.save
    #@config = record.get_value || {}
    @config = (load_config || {}).merge(hash)
    MiscConfig.save_config(ConfigKey, @config)
  end

  def initialize(mode, live_program_klass=LiveProgram)
    case mode
    when 'week'
      @range = 0..7
      @step = 1
    when 'month'
      @range = 8..31
      @step = 3
    when 'far'
      @range = 32..90
      @step = 7
    when 'search'
      # do nothing
    else
      raise "unrecognized mode: #{mode}"
    end
    @mode = mode
    @cycle_key = "#{@mode}_cycle"
    #@config = load_config
    load_config
    @today = Date.today
    
    @live_program_klass = live_program_klass
  end

  def next_offset(offset)
    offset += 1
    offset = @range.min if @range.max < offset
    offset
  end

  def crawl_day(offset, only_new)
    #only_new = true unless @mode == 'week'  # XXX: quick hack
    #@live_program_klass.crawl_timetable(@today + offset, only_new: only_new)
    @live_program_klass.crawl_timetable2(@today + offset, only_new: only_new)
  end

  def execute
    if @mode == 'search'
      @live_program_klass.crawl_from_search
    else
      offset = @config[@mode] || @range.min
      update_config({@cycle_key => 0}) unless @config[@cycle_key]
      @step.times{
        crawl_day(offset, @config[@cycle_key] != 0)
        offset = next_offset(offset)
        config = { @mode => offset }
        if offset == @range.min
          config[@cycle_key] = (@config[@cycle_key] + 1) % 6
        end
        update_config(config)
      }
    end
  end
end


