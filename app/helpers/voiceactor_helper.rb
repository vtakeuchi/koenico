module VoiceactorHelper

  def voiceactor_name(name_or_va)
    name_or_va.kind_of?(Voiceactor) ? name_or_va.name : name_or_va
  end

  def link_to_voiceactor(name_or_va)
    name = voiceactor_name(name_or_va)
    content_tag(:span, name,
        data: {'voiceactor-name' => name,
               'koenico-ujs' => 'voiceactor-fav'})
  end
end
