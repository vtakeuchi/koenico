module ProgramsHelper
  def japanese_wday(time)
    %w(日 月 火 水 木 金 土)[time.wday]
  end

  def full_time_to_s(time)
    time.in_time_zone('Tokyo').strftime("%Y/%m/%d(#{japanese_wday(time)}) %H:%M")
  end

  def provider_classname(program)
    {
      'channel' => 'program-provider-channel',
      'official' => 'program-provider-official'
    }[program.provider_type] || 'program-provider-unknown'
  end

  def status_label(program, **opt)
    make_tag = lambda{| label,kind |
      content_tag('SPAN', label, class: "program-status program-status-#{kind}", **opt)
    }
    case
    when program.finished?
      make_tag.('放送終了', :finished)
    when program.onair?
      make_tag.('放送中', :onair)
    when program.started?(Time.now.in_time_zone('Tokyo').end_of_day + 6.hours + 5.minutes)
      make_tag.('もうすぐ', :soon)
    else
      nil
    end
  end

  def will_paginate_programs(programs)
    will_paginate programs, previous_label: '≪', next_label: '≫'
  end

  def pagination_info(programs)
    if programs.total_entries > 0
      "全#{programs.total_entries}件見つかりました。"
    else
      '見つかりませんでした。'
    end
  end

  # generate finished/unfinished version of the current path
  def programs_version_path(finished)
    action = (/_finished\z/ =~ params[:action]) ? $` : params[:action]
    action += '_finished' if finished
    url_for(action: action)
  end
end
