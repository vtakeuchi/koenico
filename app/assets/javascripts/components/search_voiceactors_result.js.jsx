//= require 'voiceactor'

var SearchVoiceactorsResult = React.createClass({
  propTypes: {
    curatorForVoiceactor: React.PropTypes.instanceOf(ReactGreen.Curator),
    placeholderText: React.PropTypes.string,
    notFoundText: React.PropTypes.string,
  },

  mixins: [ReactGreen.Mixin],

  /* state:
   *   names: Array of String
   */

  componentWillMount: function(){
    /*
    this.placeholder = this.props.placeholderText || '検索結果が表示されます';
    this.notFound = this.props.notFoundText || '見つかりませんでした';
    */
    this.placeholder = this.props.placeholderText;
    this.notFound = this.props.notFoundText;
  },

  makeVoiceactorFavElement: function(name){
    return this.getCurator().reactGreen.attachCurator(
      <VoiceactorFav name={name} />,
      this.props.curatorForVoiceactor
    );
  },

  render: function(){
    if (this.state.names == null){
      if (this.placeholder){
        return <div className="alert alert-info">{this.placeholder}</div>;
      }else{
        return <div />;
      }
    }else if(this.state.names.length == 0){
      if (this.notFound){
        return <div className="alert alert-danger">{this.notFound}</div>;
      }else{
        return <div />;
      }
    }else{
      var items = this.state.names.map(function(name){  // XXX: is key ok?
        return <li key={name}>{this.makeVoiceactorFavElement(name)}</li>;
      }.bind(this));
      return <ul className="voiceactors-list">{items}</ul>;
    }
  }
});
