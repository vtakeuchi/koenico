var SearchVoiceactorsForm = React.createClass({
  mixins: [ReactGreen.Mixin],

  handleSubmit: function(e){
    e.preventDefault();
    this.triggerEvent('submit', this.refs.inputText.value, this.refs.prefixCheckbox.checked);
  },

  handleChange: function(e){
    this.triggerEvent('change', this.refs.inputText.value, this.refs.prefixCheckbox.checked);
  },

  render: function(){
    return <form action='#' method='GET' className='form-inline' onSubmit={this.handleSubmit}>
      <label>
        <input name="search-voiceactors-input" type='text' placeholder='声優名で検索'
          className='form-control' ref='inputText' onChange={this.handleChange} />
      </label>
      <label>
        <span className="sr-only">検索</span>
        <input type='submit' value='検索' className='form-control' />
      </label>
      <label><input type='checkbox' defaultChecked="checked" className='form-control'
          ref='prefixCheckbox' onChange={this.handleChange}/> 前方一致</label>
    </form>
  }
})
