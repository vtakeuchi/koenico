//= require 'voiceactor'
var VoiceactorFav = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
  },

  /* state:
   *   favorites: Array of voiceactor names
   */

  /* events:
   *   change(event, name, favved)
   */

  mixins: [ReactGreen.Mixin],

  getFavved: function(props, state){
    return (state.favorites || []).indexOf(props.name) != -1;
  },

  shouldComponentUpdate: function(nextProps, nextState){
    return this.favved != this.getFavved(nextProps, nextState);
  },

  handleClick: function(e){
    if (e.button == 0){ // left button?
      e.preventDefault();
      this.triggerEvent('change', this.props.name, !this.favved);
    }
  },

  render: function(){
    this.favved = this.getFavved(this.props, this.state);
    return <span>
      <VoiceactorFav.Name {...this.props} />
      <VoiceactorFav.Button {...this.props} favved={this.favved} onClick={this.handleClick} />
    </span>
  }
});

VoiceactorFav.Name = React.createClass({
  render: function(){
    var name = this.props.name;
    return <a href={ Routes.programs_search_by_voiceactors_path({ names: name })}
      className='voiceactor-link' >{name}</a>
  }
});

VoiceactorFav.Button = React.createClass({
  componentWillMount: function(){
  },

  render: function(){
    var common_props = {
      onClick: this.props.onClick,
      href: Routes.profile_favorites_path()
    };
    if (this.props.favved){
      return <VoiceactorFav.Button.Favved {...common_props} />
    }else{
      return <VoiceactorFav.Button.UnFavved {...common_props} />
    }
  }
});

VoiceactorFav.Button.Favved = React.createClass({
  render: function(){
    return <a className="fav-button fav-button-favved" title="お気に入り解除" {...this.props}>
      <span className="glyphicon glyphicon-heart" /></a>
  }
});

VoiceactorFav.Button.UnFavved = React.createClass({
  render: function(){
    return <a className="fav-button fav-button-unfavved" title="お気に入り登録" {...this.props}>
      <span className="glyphicon glyphicon-heart-empty" /></a>
  }
});
