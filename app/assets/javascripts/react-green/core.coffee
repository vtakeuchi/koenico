##
# sample:
#
# green = new ReactGreen
#   searchResult: null
#   otherData: null
#
# reactGreen.registerEvent SearchFormComponent, 'submit-triggered', (e, search_pattern) ->
#   get_result_by_ajax(search_pattern).then (result) ->
#     reactGreen.update
#       searchResult: result
#
# searchResultDisplayCurator = reactGreen.makeCurator (data) ->
#   items: data.searchResult
#
# ReactDOM.render(
#   searchResultDisplayCurator.createAttachedElement(
#     SearchResultDisplayComponent,
#       placeholderForInput: 'Search something!'  # designate props
#   ),
#   document.getElementById('search-result-container')
# )
class @ReactGreen
  constructor: (data) ->
    @data = data
    @components = new Set
    @eventRegistory = new ReactGreen.EventRegistory

  ## public methods

  update: (data) ->
    $.extend(@data, data)
    @dispatchDataChanged(data)
    this
      
  makeCurator: (f) ->
    ReactGreen.Curator.make(this, f)

  makeNullCurator: ->
    @makeCurator -> {}

  registerEvent: (klass, type, listener) ->
    @eventRegistory.register(klass, type, listener)

  resetComponents: ->
    @components = new Set

  renderElement: (react_element, curator, container) ->
    ReactDOM.render @attachCurator(react_element, curator), container

  renderComponent: (comp_klass, props, children, curator, container) ->
    @renderElement(
      React.createElement(comp_klass, props, children),
      curator,
      container
    )

  attachCurator: (element, curator) ->
    # XXX: element.type undocumented
    (curator ?
      element.type.getDefaultCurator?() ?
      @makeNullCurator()
    ).attachToReactElement(element)

  getStoredData: ->
    @data

  ## private methods

  dispatchDataChanged: (data_updated) ->
    @components.forEach (comp) =>
      if (comp.isMounted())
        comp.setState(comp.getCurator().processBubblingUp(@data))

  subscribeComponent: (comp) ->
    @components.add(comp)
    this

  unsubscribeComponent: (comp) ->
    @components.delete(comp)
    this


  @Mixin:
    getCurator: ->
      @props._reactGreenCurator

    triggerEvent: (type, params...) ->
      # XXX: .one supported?
      $(this).one type, (e, ext) =>
        @getCurator().reactGreen.eventRegistory.trigger(this, type, e, ext...)
      $(this).triggerHandler type, [params]

    componentWillMount: ->
      #@parentCurator = @props._reactGreenCurator

    componentDidMount: ->
      @getCurator().reactGreen.subscribeComponent(this)

    componentWillMount: ->
      @getCurator().reactGreen.unsubscribeComponent(this)

    getInitialState: ->
      @getCurator().getCurrentState()


class ReactGreen.Curator
  @make: (rg, f) ->
    new (
      class extends this
        process: f
    )(rg)

  constructor: (rg, props) ->
    @reactGreen = rg
    #@props = props
    @parentCurator = null

  addChild: (child) ->
    child.parentCurator = this

  getCurrentState: ->
    @processBubblingUp(@reactGreen.getStoredData())

  processBubblingUp: (data) ->
    @process(
      if @parentCurator
        @parentCurator.processBubblingUp(data)
      else
        data
    )

  # abstract method:
  #   return React state or data passed to children
  process: (pdata) ->
    pdata
  
  attachToReactElement: (react_elt) ->
    React.cloneElement react_elt,
      _reactGreenCurator: this

  createAttachedElement: (comp, props, children) ->
    @attachToReactElement(React.createElement(comp, props, children))

# manage 'automatic' event listners
class ReactGreen.EventRegistory
  constructor: ->
    @listenerMap = [] # array of [klass, type, listener]

  register: (klass, type, listener) ->
    @listenerMap.push([klass, type, listener])

  trigger: (comp, triggered_type, params...) ->
    for [klass,type,listener],_ in @listenerMap
      if (comp instanceof klass) and (triggered_type == type)
        listener.call(comp, params...)
