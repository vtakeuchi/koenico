KoeNico.Util =
  onPageReady: (callback) ->
    $(document).on 'turbolinks:load', callback

  onPageLeave: (callback) ->
    $(document).on 'turbolinks:visit', callback

