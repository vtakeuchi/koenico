KoeNico.API =
  execute: (type, name, data, args) ->
    $.ajax(
      type: type
      url: Routes["#{name}_path"].call(Routes,
        $.extend({format: 'json'}, args))
      dataType: 'json'
      data: data
    ).fail( (xhr,status,e) ->
      console.error("KoeNicoAPI failed: #{status} #{e}")
    )

  get: (name, args) -> @execute('GET', name, null, args)
  post: (name, data, args) -> @execute('POST', name, data, args)
  put: (name, data, args) -> @execute('PUT', name, data, args)
  delete: (name, args) -> @execute('DELETE', name, null, args)
