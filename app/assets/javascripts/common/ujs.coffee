#= require './util'

KoeNico.Util.onPageReady ->
  KoeNico.UJS.run()

KoeNico.UJS =

  handlers: []

  # handler is an Object that has these (optional) methods:
  #   init: called before document is scanned
  #   setup: called each time specified elt is found
  #   final: called after document is scanned
  register: (key, handler) ->
    @handlers.push([key, handler])

  run: ->
    handler.init?() for [_,handler],_ in @handlers

    $('[data-koenico-ujs]').each (_,elt) =>
      handlers = (h for [key,h],_ in @handlers when key == elt.dataset.koenicoUjs)
      if handlers.length > 0
        handler.setup?(elt) for handler in handlers
      else
        console.warn("handler for #{elt.dataset.koenicoUjs} not found")

    handler.final?() for [_,handler],_ in @handlers
