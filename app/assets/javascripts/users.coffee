# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@KoeNico.Users =
  queryFavorites: ->
    KoeNico.API.get('profile_favorites').done (data) ->
      data.favorites

  loadVoiceactorData: ->
    if KoeNico.Voiceactors
      $.Deferred().resolve(KoeNico.Voiceactors).promise()
    else
      KoeNico.API.get('voiceactors').done (data) ->
        # TODO: save data to local
        KoeNico.Voiceactors = data.voiceactors
