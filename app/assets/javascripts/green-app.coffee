forceArray = (obj) ->
  if obj instanceof Array
    obj
  else
    []

addToSearchHistory = (name) ->
  history = reactGreen.getStoredData().searchVoiceactorsHistory
  if history.indexOf(name) == -1 and not initialFavorites.asSet.has(name)
    history.concat([name])
  else
    null


initialFavorites = forceArray(gon?.favorites)
initialFavorites.asSet = new Set(initialFavorites)

reactGreen = new ReactGreen
  favorites: initialFavorites
  searchVoiceactorsResult: null
  searchVoiceactorsHistory: []

KoeNico.global.reactGreen = reactGreen # for debugging

KoeNico.Util.onPageLeave ->
  # if you don't do this, React components keep stacking up when you turbolink
  reactGreen.resetComponents()

# HACK: dirty
###
unless gon?
  KoeNico.Util.onPageReady ->
    KoeNico.API.get('profile_favorites').then (data, status, xhr) ->
      reactGreen.update
        favorites: data.favorites
###

curators =
  voiceactors: reactGreen.makeCurator (data) ->
    favorites: data.favorites

  searchVoiceactorsResult: reactGreen.makeCurator (data) ->
    names: data.searchVoiceactorsResult

  searchVoiceactorsHistory: reactGreen.makeCurator (data) ->
    names: data.searchVoiceactorsHistory

VoiceactorFav.getDefaultCurator = -> curators.voiceactors


## event registrations
reactGreen.registerEvent VoiceactorFav, 'change', (e, name, fav) ->
  (
    if fav
      KoeNico.API.put('profile_favorite', '', {name: name})
    else
      KoeNico.API.delete('profile_favorite', {name: name})
  ).done( (data, status, xhr) ->
    hash = { favorites: data.favorites }
    # console.log(addToSearchHistory(name)) if fav
    if fav and history = addToSearchHistory(name)
      hash.searchVoiceactorsHistory = history
    reactGreen.update hash
  )

reactGreen.registerEvent SearchVoiceactorsForm, 'submit', (e,name, prefix) ->
  KoeNico.VoiceactorsData.loaded().then (data) ->
    reactGreen.update
      searchVoiceactorsResult: (va.name for va in data.searchVoiceactors(name, prefix))

do (timerID=null) ->
  reactGreen.registerEvent SearchVoiceactorsForm, 'change', (e,name, prefix) ->
    KoeNico.VoiceactorsData.loaded().then (data) ->
      clearTimeout(timerID) if timerID?
      timerID = setTimeout ->
        names =
          if name.trim() == ''
            null
          else
            va.name for va in data.searchVoiceactors(name, prefix).sort (v1,v2) ->
              if v1.furigana > v2.furigana
                1
              else if v1.furigana == v2.furigana
                0
              else
                -1

        reactGreen.update
          searchVoiceactorsResult: names
      , 150


## component renderings

KoeNico.UJS.register 'voiceactor-fav',
  setup: (container) ->
    reactGreen.renderComponent(VoiceactorFav,
      {name: container.dataset.voiceactorName},
      null, null, container
    )

KoeNico.UJS.register 'voiceactors-search-form-container',
  setup: (container) ->
    reactGreen.renderComponent(SearchVoiceactorsForm, null, null, null, container)

  final: ->
    if sr_container = document.getElementById('voiceactor-search-result-container')
      reactGreen.renderComponent(SearchVoiceactorsResult,
        {
          placeholderText: '検索結果が表示されます'
          notFoundText: '見つかりませんでした'
        }, null, curators.searchVoiceactorsResult, sr_container)
    if sh_container = document.getElementById('voiceactor-search-history-container')
      reactGreen.renderComponent(SearchVoiceactorsResult,
        null, null, curators.searchVoiceactorsHistory, sh_container)
