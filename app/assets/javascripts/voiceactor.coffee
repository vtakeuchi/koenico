# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# convert katakana to hiragana
normalizeFurigana = (str) ->
  str.replace /[\u30a1-\u30f6]/g, (match) ->
    String.fromCharCode(match.charCodeAt(0) - 0x60)


class KoeNico.VoiceactorsData
  @data: null
  @data_promise: null

  # return Promise that passes VoiceactorsData instance
  @loaded: ->
    if @data?
      $.Deferred().resolve(new this).promise()
    else if @data_promise
      @data_promise
    else
      @data_promise = KoeNico.API.get('voiceactors').then(
        (json_data) =>
          # TODO: save data to local
          @data = json_data.voiceactors
          new this
        ,(reason) =>
          console.error(reason)
          @data = []
          new this
      )

  getData: ->
    @constructor.data

  execStringMatch: (str, pattern, prefix) ->
    if prefix
      str.startsWith(pattern)
    else
      str.indexOf(pattern) != -1

  searchVoiceactors: (pattern, prefix) ->
    pattern = pattern.trim()
    return [] if pattern == ''
    pattern_kana = normalizeFurigana(pattern)
    @getData().filter (va) =>
      @execStringMatch(va.name, pattern, prefix) or
        (va.furigana && @execStringMatch(va.furigana, pattern_kana, prefix))
