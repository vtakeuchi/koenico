json.extract! manage_voiceactor, :id, :name, :furigana, :gender, :abstract, :created_at, :updated_at
json.url manage_voiceactor_url(manage_voiceactor, format: :json)