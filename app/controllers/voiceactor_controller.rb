class VoiceactorController < ApplicationController
  def index
    @voiceactors = Voiceactor.all
    respond_to{| format |
      format.html
      format.json{ render json: {
          voiceactors: Voiceactor.friendly_data
        }
      }
    }
  end

  def search_by_names
    # XXX: check nil
    names = params[:names].split(',').map(&:strip)
    @voiceactors = Voiceactor.where('name in (?) OR furigana in (?)', names, names)
    render :index
  end
end
