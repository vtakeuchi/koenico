class UsersController < ApplicationController
  def list_favorites
    respond_to{| format |
      format.html
      format.json{ render json: {favorites: @favorites} }
    }
  end

  def add_favorite
    name = normalize_voiceactor_name(params[:name])
    @favorites |= [name]
    store_favorites(@favorites)
    respond_to{| format |
      format.html{ head :no_content }
      format.json{ render json: {status: 'added', favorites: @favorites} }
    }
  end

  def delete_favorite
    name = normalize_voiceactor_name(params[:name])
    @favorites -= [name]
    store_favorites(@favorites)
    respond_to{| format |
      format.html{ head :no_content }
      format.json{ render json: {status: 'deleted', favorites: @favorites} }
    }
  end

  private
  def set_cookie_favorites(names)
    cookies[:voiceactors_favorites] = names.join(',')
  end

end
