class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :require_admin_authenticate
  before_action :set_favorites

  def as_int(v, default=nil)
    Integer(v)
  rescue
    default
  end

  def parse_name_list(s)
    if s
      s.split(',').map(&:strip)
    else
      []
    end
  end

  def normalize_voiceactor_name(name)
    name.gsub(/\p{White_Space}+/, '')
  end

  def set_favorites
    # XXX: stub
    #@favorites = parse_name_list((cookies[:voiceactors_favorites] || '').strip)
    #@favorites = session[:voiceactors_favorites]
    gon.favorites = @favorites =
      cookies.permanent.signed[:voiceactors_favorites] || []
  end

  def store_favorites(names)
    # XXX: stub
    #session[:voiceactors_favorites] = names
    cookies.permanent.signed[:voiceactors_favorites] ||= []
    cookies.permanent.signed[:voiceactors_favorites] = names
  end


  def require_admin_authenticate
    if %r{\A/admin/} =~ request.path
      authenticate_or_request_with_http_digest{| name |
        name == 'admin_leroy' and Rails.application.secrets.admin_password
      }
    end
  end
end
