class ContactsController < ApplicationController

=begin
  before_action :require_admin_authenticate, except: [
    :public_form,
    :public_report_form,
    :public_post
  ]
=end

  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  ## public actions (that is, users can see them)
  def public_form
    @category = 'contact'
    @prompt = 'ご感想やご質問があればどうぞ。'
    #@placeholder = ''
    @contact = Contact.new
    @contact.category = 'contact'
  end

  def public_report_form
    @category = 'report'
    @prompt = '登録されていない声優・番組があればご報告お願いします。'
    #@placeholder = '「」が登録されていません。'
    @contact = Contact.new
    @contact.category = 'report'
    @contact.message = <<~EOT
      登録されていない声優(複数記述可):


      登録されていない番組(複数記述可):


    EOT

    render :public_form
  end

  def public_post
    @contact = Contact.new(contact_params)
    if @contact.save
      thanks = (@contact.category == 'report' ? 'ご協力ありがとうございます。' : '')
      @notice = "送信しました。#{thanks}" 
      @alert = 'success'
    else
      @notice = 'エラー'
      @alert = 'warning'
    end
  end

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:category, :email, :message)
    end
end
