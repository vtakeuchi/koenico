class ManageVoiceactorsController < ApplicationController

  before_action :set_manage_voiceactor, only: [:show, :edit, :update, :destroy]

  # GET /manage_voiceactors
  # GET /manage_voiceactors.json
  def index
    @manage_voiceactors = ManageVoiceactor.all.order('id desc')
  end

  # GET /manage_voiceactors/1
  # GET /manage_voiceactors/1.json
  def show
    respond_to{| f |
      f.html
      f.json
      f.yaml{ render text: @manage_voiceactor.to_yaml }
    }
  end

  # GET /manage_voiceactors/new
  def new
    @manage_voiceactor = ManageVoiceactor.new
  end

  # GET /manage_voiceactors/1/edit
  def edit
  end

  # POST /manage_voiceactors
  # POST /manage_voiceactors.json
  def create
    @manage_voiceactor = ManageVoiceactor.new(manage_voiceactor_params)
    #@voiceactor = Voiceactor.new(manage_voiceactor_params)

    LiveProgram.search_missing_voiceactor(@manage_voiceactor.name).each{| program |
      program.voiceactors += [@manage_voiceactor]
      program.save
    }

    respond_to do |format|
      if @manage_voiceactor.save
        format.html { redirect_to @manage_voiceactor, notice: 'Manage voiceactor was successfully created.' }
        format.json { render :show, status: :created, location: @manage_voiceactor }
      else
        format.html { render :new }
        format.json { render json: @manage_voiceactor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /manage_voiceactors/1
  # PATCH/PUT /manage_voiceactors/1.json
  def update
    respond_to do |format|
      if @manage_voiceactor.update(manage_voiceactor_params)
        format.html { redirect_to @manage_voiceactor, notice: 'Manage voiceactor was successfully updated.' }
        format.json { render :show, status: :ok, location: @manage_voiceactor }
      else
        format.html { render :edit }
        format.json { render json: @manage_voiceactor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /manage_voiceactors/1
  # DELETE /manage_voiceactors/1.json
  def destroy
    @manage_voiceactor.destroy
    respond_to do |format|
      format.html { redirect_to manage_voiceactors_url, notice: 'Manage voiceactor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import_yaml_form
  end

  def import_yaml
    messages = Voiceactor.import_errata_from_yaml(params[:yaml])
    render plain: messages.join("\n")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manage_voiceactor
      @manage_voiceactor = ManageVoiceactor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def manage_voiceactor_params
      params.require(:manage_voiceactor).permit(:name, :furigana, :gender, :abstract)
    end
end
