class ProgramsController < ApplicationController
  module ParamsTruthValue
    def yes?(key)
      not no?(key)
    end

    def no?(key)
      falsy_param?(self[key])
    end

    module_function
    def falsy_param?(val)
      val.blank? or /\A\s*(?:0+|false|no)\s*\z/i =~ val
    end
  end

  class ::ActionController::Parameters
    include ParamsTruthValue
  end

  def self.with_finished(mname)
    define_method("#{mname}_finished"){
      params[:finished] = '1'
      __send__(mname)
    }
  end

  with_finished def index
    @header_label = '番組一覧'
    @programs = paginate(timeflow(LiveProgram.only_voiceactors_on).preloaded)
    render :index  # NOTE: needed in case the '_finished' version is called
  end

  # mainly for debugging
  def index_all
    @programs = paginate(LiveProgram.preloaded)
    render :index
  end

  with_finished def index_favorites
    @header_label = 'お気に入り'
    if not @favorites or @favorites.empty?
      render :no_favorite
    else
      @programs = paginate(timeflow(query_by_voiceactor_names(@favorites)).preloaded)
      render :index
    end
  end

  with_finished def search_by_voiceactors
    @header_label = "「#{params[:names]}」"
    # XXX: check blank
    @programs = paginate(timeflow(query_by_voiceactor_names(parse_name_list(params[:names]))).preloaded)
    render :index
  end

  with_finished def search_by_tag
    @header_label = "タグ\"#{params[:tag]}\""
    # XXX: check blank
    @programs = paginate(timeflow(LiveProgram.tagged(params[:tag])).preloaded(true))
    render :index
  end

  def show_live
    @program = LiveProgram.find_by(live_id: params[:live_id])
    respond_to{| f |
      f.html
      f.json{ render json: @program }
    }
  end


  private
  def paginate(relation)
    relation.paginate(**paginate_params)
  end

  def paginate_params
    {
      page: as_int(params[:page], 1),
      per_page: [as_int(params[:per_page], 20), 100].min
    }
  end

  def query_by_voiceactor_names(names)
    LiveProgram.voiceactors_named_on(names).old_to_new
  end


  def timeflow(relation, invert: params[:finished], past_in: 8.days)
    if ParamsTruthValue.falsy_param?(invert)
      relation.unfinished.old_to_new
    else
      relation.finished.past_in(past_in).new_to_old
    end
  end

end
