class TopPageController < ApplicationController
  def index
    @programs_coming_soon =
      LiveProgram.only_voiceactors_on.unfinished.old_to_new.limit(10).preloaded
  end
end
