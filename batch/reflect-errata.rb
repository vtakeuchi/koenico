def main
  $stderr.puts Voiceactor.import_errata_from_yaml(File.read('db/seeddata/errata.yaml'))
end

main
