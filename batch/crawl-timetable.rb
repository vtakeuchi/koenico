require 'date'
require 'json'
require_relative './../app/services/nico_live_crawler.rb'

# XXX: This batch is OBSOLETE, use crawl-timetable-queue.rb

=begin
Specification:

= タイムテーブルの巡回 =
== 1週間以内 ==
  10分ごとに1日分
== 1ヶ月以内 ==
  1時間ごとに3日分
== 3ヶ月以内 ==
  6時間ごとに7日分


= 放送ページの巡回 =
== 新しい放送 ==
  すぐに取得
== 一度取得した放送 ==
  6回に1回取得

=end

ActiveRecord::Base.logger = Logger.new(STDOUT)

NicoLiveCrawler.new(ARGV[0]).execute
