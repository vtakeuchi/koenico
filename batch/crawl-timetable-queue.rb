require 'date'
require 'json'
require_relative './../app/services/nico_live_crawler.rb'

if Rails.env == 'development'
  ActiveRecord::Base.logger = Logger.new(STDOUT)
end

class ScheduleQueue
  def lcm_m(nums)
    nums.inject{| lcm,v | lcm.lcm(v) }
  end

  def gcd_m(nums)
    nums.inject{| gcd,v | gcd.gcd(v) }
  end

  def compute_shcedule(everys)
    everys = everys.map(&:to_i)
    gcd = gcd_m(everys)
    lcm = lcm_m(everys)
    divideds = everys.map{| v | v / gcd }

    (lcm / gcd).times.map{| i |
      divideds.each_with_index.map{| v,kind |
        if i % v == 0
          kind
        else
          nil
        end
      }
    }.flatten.compact
  end

  def initialize(spec)
    @schedules = compute_shcedule(spec.values).map{| i | spec.keys[i] }
  end

  def execute
    cycle = MiscConfig.load_config(:CrawlScheduleCycle) || 0
    kind = @schedules[cycle]
    unless kind
      cycle = 0
      kind = @schedules[cycle]
    end
    NicoLiveCrawler.new(kind.to_s).execute
    MiscConfig.save_config(:CrawlScheduleCycle, cycle+1)
    self
  end
end


ScheduleQueue.new({
  week: 10.minutes,
  month: 1.hours,
  far: 6.hours,
  search: 6.hours,
}).execute
