require 'mechanize'
require 'uri'
require 'io/console'

def main
  host,port = ARGV[0].split(':')
  uri = URI::HTTP.build(host: host, port: port)
  agent = Mechanize.new
  agent.add_auth(uri, 'admin_leroy', get_password)
  uri.path = '/admin/manage_voiceactors/import_yaml'
  doc = agent.get(uri) # get csrf token
  doc.form_with(id: 'the-form'){| f |
    f.yaml = File.read(File.dirname(__FILE__) + '/../db/seeddata/errata.yaml')
  }.submit.body
end

def get_password
  IO.console.print('password> ')
  IO.console.gets.chomp
end


puts main
