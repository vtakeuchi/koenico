module ::KoeNicoRoutesExtender
  def get_with_finished(path, *args, **kwds)
    fkwds = {}
    fkwds[:action] = "#{kwds[:action]}_finished"
    fkwds[:as] = "#{kwds[:as]}_finished" if kwds[:as]
    get path, *args, **kwds
    get "#{path}/finished", *args, **(kwds.merge(fkwds))
  end
end

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root controller: 'top_page', action: 'index'

  scope '/programs', controller: 'programs', as: 'programs' do

    extend ::KoeNicoRoutesExtender

    get_with_finished '/',          action: 'index'
    get_with_finished '/favorites', action: 'index_favorites'

    #get '/all',       action: 'index_all'

    get_with_finished '/v/:names',
      action: 'search_by_voiceactors',
      as: :search_by_voiceactors

    get_with_finished '/tag/:tag',
      action: 'search_by_tag',
      as: :search_by_tag
  end

  get '/voiceactors', to: 'voiceactor#index'

  # TODO: make restful
  get    '/profile/favorites', to: 'users#list_favorites'
  put    '/profile/favorites/:name', to: 'users#add_favorite', as: 'profile_favorite'
  delete '/profile/favorites/:name', to: 'users#delete_favorite'

  scope '/admin' do
    get  'manage_voiceactors/import_yaml', to: 'manage_voiceactors#import_yaml_form'
    post 'manage_voiceactors/import_yaml', to: 'manage_voiceactors#import_yaml'

    resources :manage_voiceactors
    resources :contacts

    get 'programs/:live_id', to: 'programs#show_live'
  end

  get '/contact', to: 'contacts#public_form', as: 'contact_public'
  get '/contact/report', to: 'contacts#public_report_form', as: 'contact_public_report'
  post '/contact/post', to: 'contacts#public_post', as: 'contact_public_post'
end
