desc 'Counter cache'

task voiceactor_counter: :environment do
  LiveProgram.reset_column_information
  LiveProgram.find_each do | p |
    LiveProgram.reset_counters p.id, :voiceactors
  end
end
