require 'uri'
require 'faraday'
require 'json'
require 'pp'
require 'nokogiri'
require 'tapp'
require 'logger'

module NiconicoAPI
  Targets_Keyword = %w(title description tags)

  module_function

  def extract_live_id(content_id)
    if /\Alv(\d+)/ =~ content_id
      $1.to_i
    else
      nil
    end
  end

  def hash_from_result(lives)
    lives.map{| data |
      live_id = NiconicoAPI.extract_live_id(data['contentId'])
      if live_id
        [live_id, data]
      else
        #logger.warn{ "not valid contentId: #{data['contentId']}" }
        nil
      end
    }.compact.to_h
  end

  def query_nicolive_api(targets, qs, from: nil, to:nil, logger:Logger.new(STDOUT))
    targets_value = targets.join(',')
    qs_value = qs.map{| q | %<"#{q}"> }.join(' OR ')  # XXX: quote

    per_request_count = 100  # const

    con = Faraday.new(:url => 'http://api.search.nicovideo.jp/api/v2/live/contents/search'){| f |
      f.request :url_encoded
      f.adapter :net_http
    }

    lives = []
    offset = 0
    total_count = nil

    while offset == 0 or offset < total_count
      sleep 2 unless offset == 0

      res = nil
      4.times{
        begin
          res = JSON.parse(con.post{| req |
            req.headers['User-Agent'] = 'Koenico/1.0'
            req.body = {
              targets: targets_value,
              q: qs_value,
              fields: 'contentId,title,description,tags,startTime,thumbnailUrl,providerType',
              _sort: '+startTime',
              _limit: 100,
              _offset: offset,
              _context: 'koenico',
              'filters[liveStatus][0]' => 'onair',
              'filters[liveStatus][1]' => 'reserved',
              'filters[providerType][0]' => 'official',
              'filters[providerType][1]' => 'channel',
              'filters[startTime][gte]' => from&.xmlschema,
              'filters[startTime][lte]' => to&.xmlschema,
            }.reject{| k,v | v.nil? }
          }.body)
          break
        rescue Faraday::ConnectionFailed
          sleep 3
          retry
        end
      }
      break unless res

      total_count = res.dig('meta','totalCount')
      unless res.dig('meta', 'status') == 200
        logger.error 'query failed'
        logger.error{ res.pretty_inspect }
        logger.error{ qs_value }
        break
      end

      offset += per_request_count
      lives += res['data']
    end

    lives
  end
end
